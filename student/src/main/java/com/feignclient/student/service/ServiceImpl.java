package com.feignclient.student.service;

import com.feignclient.student.entity.Student;
import com.feignclient.student.entity.StudentWithSubject;
import com.feignclient.student.entity.SubjectResponse;
import com.feignclient.student.feignClient.SubjectClient;
import com.feignclient.student.repository.StudentsRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@org.springframework.stereotype.Service
public class ServiceImpl implements Service {

    @Autowired
    StudentsRepository repository;

    @Autowired
    SubjectClient client;

    @Override
    public List<Student> getAllStudents() {
        return repository.findAll();
    }

    @Override
    public Student getStudent(int id) {
        return repository.findById(id).get();
    }

    @Override
    public StudentWithSubject getStudentWithSubject(int studentId, int subjectId) {
        Student student = repository.findById(studentId).get();
        SubjectResponse subjectResponse = client.getSubjectResponse(subjectId);

        StudentWithSubject studentWithSubject = new StudentWithSubject(student, subjectResponse);
        return studentWithSubject;
    }

    @Override
    public Student saveStudent(Student student) {
        return repository.save(student);
    }

    @Override
    public SubjectResponse saveSubjectResponse(SubjectResponse response) {
        return client.saveSubjectResponse(response);
    }

    @Override
    public String deleteSubjectResponse(int id) {
        return client.deleteSubjectResponse(id);
    }

    @Override
    public String getSubjectResponsePort() {
        return client.getPort();
    }
}
