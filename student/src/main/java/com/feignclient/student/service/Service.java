package com.feignclient.student.service;

import com.feignclient.student.entity.Student;
import com.feignclient.student.entity.StudentWithSubject;
import com.feignclient.student.entity.SubjectResponse;

import java.util.List;

public interface Service {
    List<Student> getAllStudents();

    Student getStudent(int id);

    StudentWithSubject getStudentWithSubject(int studentId, int subjectId);

    Student saveStudent(Student student);

    SubjectResponse saveSubjectResponse(SubjectResponse response);

    String deleteSubjectResponse(int id);

    String getSubjectResponsePort();
}
