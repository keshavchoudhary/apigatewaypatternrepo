package com.feignclient.student.controller;

import com.feignclient.student.entity.Student;
import com.feignclient.student.entity.StudentWithSubject;
import com.feignclient.student.entity.SubjectResponse;
import com.feignclient.student.service.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class StudentController {

    @Autowired
    ServiceImpl service;

    @GetMapping("/students")
    public List<Student> getAllStudents() {
        return service.getAllStudents();
    }

    @GetMapping("/student/{id}")
    public Student getStudent(@PathVariable int id) {
        return service.getStudent(id);
    }

    @GetMapping("/studentWithSubject")
    public StudentWithSubject getStudentWithSubject(@RequestParam int studentId, @RequestParam int subjectId) {
        return service.getStudentWithSubject(studentId, subjectId);
    }

    @PostMapping("/student")
    public Student saveStudent(@RequestBody Student student) {
        return service.saveStudent(student);
    }

    @PostMapping("/subjectResponse")
    public SubjectResponse saveSubjectResponse(@RequestBody SubjectResponse response) {
        return service.saveSubjectResponse(response);
    }

    @DeleteMapping("/subjectResponse/{id}")
    public String deleteSubject(@PathVariable int id) {
        return service.deleteSubjectResponse(id);
    }

    @GetMapping("/port")
    public String getSubjectResponsePort(){
        return service.getSubjectResponsePort();
    }
}
