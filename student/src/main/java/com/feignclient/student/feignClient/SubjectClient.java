package com.feignclient.student.feignClient;

import com.feignclient.student.entity.SubjectResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

//@FeignClient(value = "subjectClient", url = "http://localhost:8085/app")
@FeignClient(name = "http://SUBJECT-SERVICE/subject")
public interface SubjectClient {

    @GetMapping("/subject/{id}")
    public SubjectResponse getSubjectResponse(@PathVariable int id);

    @PostMapping("/subject")
    public SubjectResponse saveSubjectResponse(@RequestBody SubjectResponse response);

    @DeleteMapping("/subject/{id}")
    public String deleteSubjectResponse(@PathVariable int id);

    @GetMapping("/port")
    public String getPort();
}
