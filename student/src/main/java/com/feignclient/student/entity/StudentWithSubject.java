package com.feignclient.student.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class StudentWithSubject {

    private Student student;
    private SubjectResponse subject;


}
