package com.feignclient.subject.controller;

import com.feignclient.subject.entity.Subject;
import com.feignclient.subject.repository.SubjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class Controller {

    @Autowired
    SubjectRepository repository;

    @Autowired
    Environment environment;

    @GetMapping("/subjects")
    public List<Subject> getAllSubjects() {
        return repository.findAll();
    }

    @GetMapping("/subject/{id}")
    public Subject getSubject(@PathVariable int id) {
        return repository.findById(id).get();
    }

    @PostMapping("/subject")
    public Subject saveSubject(@RequestBody Subject subject) {
        return repository.save(subject);
    }

    @DeleteMapping("/subject/{id}")
    public String deleteSubject(@PathVariable int id) {
        repository.deleteById(id);
        return "Subject Deleted";
    }

    @GetMapping("/port")
    public String getPort() {
        return "Port is - "+environment.getProperty("local.server.port");
    }

}
